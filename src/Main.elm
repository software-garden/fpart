module Main exposing (main)

import Browser
import Browser.Dom
import Browser.Events
import Dict exposing (Dict)
import Direction2d exposing (Direction2d)
import Element
import Element.Background as Background
import Element.Border as Border
import Html exposing (Html)
import Html.Attributes
import Json.Decode exposing (Decoder)
import List.Extra as List
import Point2d exposing (Point2d)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Svg.Events exposing (..)
import Task
import Vector2d exposing (Vector2d)


main : Program () State Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias State =
    { color : Color
    , rules : Rules
    , windowSize : Vector2d
    , selectedDot :
        Maybe
            { rule : Color
            , dot : Dot
            }
    , time : Float
    , paused : Bool
    }


type alias Rules =
    Dict Color (List Segment)


type alias Dot =
    { color : Color
    , center : Point2d
    }


type alias Color =
    String


type Msg
    = WindowResized Vector2d
    | DesignerClick Color Point2d
    | InsertSegemnt Color (Result Browser.Dom.Error Segment)
    | DotClicked Color Dot
    | DeselectDot
    | SelectColor Int
    | Progress Float
    | Regress Float
    | Tick Float
    | TogglePause


init : () -> ( State, Cmd Msg )
init flags =
    ( { color = "black"
      , rules =
            Dict.fromList
                [ ( "darkred", [] )
                , ( "lightblue", [] )
                , ( "olive", [] )
                , ( "orange", [] )
                ]
      , windowSize = Vector2d.zero
      , selectedDot = Nothing
      , time = 0
      , paused = True
      }
    , Browser.Dom.getViewport
        |> Task.map .viewport
        |> Task.map
            (\{ width, height } ->
                Vector2d.fromComponents ( width, height )
            )
        |> Task.perform WindowResized
    )


view : State -> Html.Html Msg
view state =
    let
        canvas =
            tree
                |> svgTree
                    [ id "canvas"
                    , viewBox "-1000 -1000 2000 2000"
                    , preserveAspectRatio "xMidYMid slice"
                    , Html.Attributes.style "width" "100%"
                    , Html.Attributes.style "height" "100%"
                    ]
                |> Element.html
                |> Element.el
                    [ Element.width Element.fill
                    , Element.height Element.fill
                    ]

        controls =
            state.rules
                |> Dict.map
                    (svgDesigner
                        []
                    )
                |> Dict.values
                |> List.map Element.html
                |> List.map
                    (Element.el
                        [ Element.width <| Element.px 200
                        , Element.height <| Element.px 200
                        , Border.color <| Element.rgb 0.3 0.3 0.3
                        , Background.color <| Element.rgba 1 1 1 0.8
                        , Border.width 1
                        , Svg.Attributes.cursor "crosshair"
                            |> Element.htmlAttribute
                        ]
                    )
                |> Element.column
                    [ Element.spacing 20
                    , Element.padding 20
                    , Element.height Element.fill
                    , Element.alignRight
                    , Element.scrollbarY
                    ]

        tree =
            Tree (Segment state.color 0 Direction2d.positiveX) []
                |> grow state.time state.rules
    in
    canvas
        |> Element.layout
            [ Element.height Element.fill
            , Element.width Element.fill
            , Element.inFront controls
            ]


update : Msg -> State -> ( State, Cmd Msg )
update msg state =
    case msg of
        WindowResized size ->
            ( { state | windowSize = size }
            , Cmd.none
            )

        DesignerClick color point ->
            let
                getRelativePoint : Browser.Dom.Element -> Point2d
                getRelativePoint { element, viewport } =
                    let
                        translation =
                            Vector2d.fromComponents
                                ( viewport.x - element.x - 100
                                , viewport.y - element.y - 100
                                )
                    in
                    Point2d.translateBy translation point
            in
            ( state
            , "designer-"
                ++ color
                |> Browser.Dom.getElement
                |> Task.map getRelativePoint
                |> Task.map (Vector2d.from Point2d.origin)
                |> Task.map Vector2d.lengthAndDirection
                |> Task.map (Maybe.withDefault ( 0, Direction2d.positiveX ))
                |> Task.map
                    (\( length, direction ) ->
                        Segment state.color length direction
                    )
                |> Task.attempt (InsertSegemnt color)
            )

        InsertSegemnt color (Ok segment) ->
            let
                rules =
                    state.rules
                        |> Dict.get color
                        |> Maybe.withDefault []
                        |> (::) segment
                        |> (\list -> Dict.insert color list state.rules)
            in
            ( { state
                | rules = rules
              }
            , Cmd.none
            )

        InsertSegemnt color (Err error) ->
            color
                |> always ( state, Cmd.none )

        DeselectDot ->
            ( { state | selectedDot = Nothing }, Cmd.none )

        SelectColor input ->
            let
                index =
                    modBy (Dict.size state.rules) (input - 1)

                color =
                    state.rules
                        |> Dict.keys
                        |> List.drop index
                        |> List.head
                        |> Maybe.withDefault "black"
            in
            ( { state | color = color }, Cmd.none )

        DotClicked color dot ->
            ( { state | selectedDot = Just { rule = color, dot = dot } }
            , Cmd.none
            )

        Progress delta ->
            ( { state | time = state.time + delta }
            , Cmd.none
            )

        Regress delta ->
            let
                time =
                    Basics.max (state.time - delta) 0
            in
            ( { state | time = time }
            , Cmd.none
            )

        Tick delta ->
            ( { state
                | time =
                    if state.paused then
                        state.time

                    else
                        state.time + Basics.min delta 16 / 1000
              }
            , Cmd.none
            )

        TogglePause ->
            ( { state | paused = not state.paused }
            , Cmd.none
            )


subscriptions : State -> Sub Msg
subscriptions state =
    let
        handleResize width height =
            ( toFloat width
            , toFloat height
            )
                |> Vector2d.fromComponents
                |> WindowResized

        handleKeyPress : Decoder Msg
        handleKeyPress =
            Json.Decode.field "key" Json.Decode.string
                |> Json.Decode.andThen
                    (\key ->
                        case key of
                            "Escape" ->
                                Json.Decode.succeed DeselectDot

                            "," ->
                                Json.Decode.succeed (Regress 0.01)

                            "." ->
                                Json.Decode.succeed (Progress 0.01)

                            "<" ->
                                Json.Decode.succeed (Regress 1)

                            ">" ->
                                Json.Decode.succeed (Progress 1)

                            "/" ->
                                Json.Decode.succeed TogglePause

                            _ ->
                                String.toInt key
                                    |> Maybe.map SelectColor
                                    |> Maybe.map Json.Decode.succeed
                                    |> Maybe.withDefault (Json.Decode.fail "Unknown key press")
                    )
    in
    Sub.batch
        [ Browser.Events.onResize handleResize
        , Browser.Events.onKeyPress handleKeyPress
        , Browser.Events.onAnimationFrameDelta Tick
        ]


type alias Connection =
    { start : Dot
    , end : Dot
    }


svgConnection : Connection -> Svg Msg
svgConnection ({ start, end } as connection) =
    let
        vector =
            Vector2d.from start.center end.center

        length =
            vector
                |> Vector2d.length
                |> String.fromFloat

        -- This is interesting, but needs more work
        -- width =
        --     vector
        --         |> Vector2d.length
        --         |> (\l -> l / 20)
        --         |> String.fromFloat
        rotation =
            vector
                |> Vector2d.direction
                |> Maybe.withDefault Direction2d.positiveX
                |> Direction2d.toAngle
                |> (\angle -> angle * 180 / pi)
                |> String.fromFloat

        transformation =
            ""
                ++ "translate( "
                ++ stringX start
                ++ ", "
                ++ stringY start
                ++ ") "
                ++ "rotate("
                ++ rotation
                ++ ") "
                ++ "scale("
                ++ length
                ++ ", "
                ++ "1"
                -- ++ width
                ++ ") "
    in
    line
        [ x1 "0"
        , y1 "0"
        , x2 "1"
        , y2 "0"
        , stroke ("url(#" ++ gradientId ( start.color, end.color ) ++ ")")
        , strokeWidth "6"
        , transform transformation
        ]
        []


gradientId : ( Color, Color ) -> String
gradientId ( start, end ) =
    "connection-" ++ start ++ "-" ++ end


stringX : Dot -> String
stringX { center } =
    center
        |> Point2d.xCoordinate
        |> String.fromFloat


stringY : Dot -> String
stringY { center } =
    center
        |> Point2d.yCoordinate
        |> String.fromFloat


svgGradient : ( Color, Color ) -> Svg Msg
svgGradient (( start, end ) as colors) =
    linearGradient
        [ id (gradientId colors)
        , x1 "0"
        , y1 "0"
        , x2 "1"
        , y2 "0"
        , gradientUnits "userSpaceOnUse"
        ]
        [ stop
            [ stopColor start, offset "0.2" ]
            []
        , stop
            [ stopColor end, offset "0.8" ]
            []
        ]


svgDot : List (Attribute Msg) -> Dot -> Svg Msg
svgDot attributes ({ center, color } as dot) =
    circle
        ([ cx (stringX dot)
         , cy (stringY dot)
         , r "6"
         , fill color
         ]
            ++ attributes
        )
        []


svgDesigner : List (Attribute Msg) -> Color -> List Segment -> Svg Msg
svgDesigner attributes color segments =
    let
        clickDecoder : Decoder Msg
        clickDecoder =
            Json.Decode.map2
                Tuple.pair
                (Json.Decode.field "clientX" Json.Decode.float)
                (Json.Decode.field "clientY" Json.Decode.float)
                |> Json.Decode.map Point2d.fromCoordinates
                |> Json.Decode.map (DesignerClick color)

        root =
            Segment color 0 Direction2d.positiveX

        tree =
            segments
                |> List.map (\segment -> Tree segment [])
                |> Tree root
    in
    svgTree
        ([ id ("designer-" ++ color)
         , viewBox "-100 -100 200 200"
         , on "click" clickDecoder
         ]
            ++ attributes
        )
        tree


grow : Float -> Rules -> Tree -> Tree
grow age rules (Tree root children) =
    if age > 0 then
        let
            angle =
                Direction2d.toAngle root.direction
        in
        rules
            |> Dict.get root.color
            |> Maybe.withDefault []
            |> List.map
                (\segment ->
                    { segment
                        | length = segment.length * age
                        , direction = Direction2d.rotateBy angle segment.direction
                    }
                )
            |> List.map (\segment -> Tree segment [])
            |> List.map (grow (age - 1) rules)
            |> Tree root

    else
        Tree root []


type Tree
    = Tree Segment (List Tree)


type alias Segment =
    { color : Color
    , length : Float
    , direction : Direction2d
    }


connect : Maybe Dot -> Tree -> List Connection
connect origin (Tree root children) =
    let
        vector =
            Vector2d.withLength root.length root.direction
    in
    case origin of
        Nothing ->
            let
                start =
                    Point2d.origin
                        |> Point2d.translateBy vector
                        |> Dot root.color
            in
            children
                |> List.map (connect (Just start))
                |> List.concat

        Just start ->
            let
                end =
                    start.center
                        |> Point2d.translateBy vector
                        |> Dot root.color
            in
            children
                |> List.map (connect (Just end))
                |> List.concat
                |> (::) (Connection start end)


treeToDots : Point2d -> Tree -> List Dot
treeToDots origin (Tree root children) =
    let
        vector =
            Vector2d.withLength root.length root.direction

        newOrigin =
            origin
                |> Point2d.translateBy vector
    in
    children
        |> List.map (treeToDots newOrigin)
        |> List.concat
        |> (::) (Dot root.color newOrigin)


svgTree : List (Svg.Attribute Msg) -> Tree -> Svg Msg
svgTree attributes tree =
    svg attributes
        [ defs []
            (tree
                |> connect Nothing
                |> List.map
                    (\{ start, end } ->
                        ( start.color, end.color )
                    )
                |> List.unique
                |> List.map svgGradient
            )
        , g
            []
            (tree
                |> connect Nothing
                |> List.map svgConnection
            )
        , g []
            (tree
                |> treeToDots Point2d.origin
                |> List.reverse
                |> List.map (svgDot [])
            )
        ]
